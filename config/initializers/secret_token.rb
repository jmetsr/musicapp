# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MusicApp::Application.config.secret_key_base = '706b36975567010bb6ce6557dbfc743d56fe89f1ae76e8259e83db34fba135bc509d39baa2e65122a631fd3b12c14ce53d57c362585954f18c5851a9e7e4cb39'
