class Album < ActiveRecord::Base
  has_many(
  :tracks,
  :class_name => "Track",
  primary_key: :id,
  foreign_key: :album_id,
  before_destroy :dependent => :destroy
  )

  belongs_to(
  :band,
  :class_name => "Band",
  primary_key: :id,
  foreign_key: :band_id
  )

end