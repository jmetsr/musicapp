class Band < ActiveRecord::Base
  attr_accessor :name
  has_many(
    :albums,
    :class_name => "Album",
    primary_key: :id,
    foreign_key: :album_id
   # before_destroy :dependent => :destroy
  )
  before_destroy :dependent => :destroy
  validates :name, presence: true
end