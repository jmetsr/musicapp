require 'bcrypt'
class User < ActiveRecord::Base
  include BCrypt
  attr_reader :password

  after_initialize do |user|
    user.ensure_session_token
  end

  validates :email_address, presence: true
  validates :password_digest, presence: true
  validates :password, :length => {minimum: 1, :allow_nil => true}


  def self.find_by_credentials(email, password)
    matching_users = []
    User.all.each do |user|
      pswords_match = user.is_password?(password)
      if user.email_address == email && pswords_match
        matching_users << user
      end
    end

    return matching_users[0]
  end

  def self.generate_session_token
    SecureRandom.urlsafe_base64
  end

  def reset_session_token!
    self.session_token = User.generate_session_token
  end

  def ensure_session_token
    self.session_token ||= User.generate_session_token
  end

  def password=(password)
    pword = Password.create(password)
    self.password_digest = pword
    @password = password
  end

  def is_password?(password)
    db_password = Password.new(self.password_digest)
    db_password == password
  end

end
