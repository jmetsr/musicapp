class BandsController < ApplicationController
  def create
    @band = Band.new(band_params)
    if @band.save
      redirect_to band_url(@band)
    else
      render action: 'new'
    end
  end

  def show
    @band = Band.find(params[:id])
    render :show
  end

  def index
    @bands = Band.all
  end

  private
  def band_params
    params.require(:band).permit(:name)
  end

end