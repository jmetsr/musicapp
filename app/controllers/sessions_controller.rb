class SessionsController < ApplicationController
  def index

  end
  def new

  end

  def create
    @user = User.find_by_credentials(*params["user"].values)
    if @user != nil
      login!(@user)
      redirect_to "http://localhost:3000/users/#{@user.id}"
    else
      flash[:notice] = "invalid password or username"
      redirect_to "http://localhost:3000/sessions"
    end
  end

  def destroy
    logout!(current_user)
    redirect_to users_url
  end

  def show
  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.permit(:email_address, :password)
    end
end