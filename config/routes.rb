MusicApp::Application.routes.draw do
  resources :users
  resource :sessions
  resources :bands
  resources :albums
  resources :tracks

end
